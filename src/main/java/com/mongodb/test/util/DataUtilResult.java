package com.mongodb.test.util;

import java.io.Serializable;

public class DataUtilResult<T> implements Serializable {

    private int code;
    private String message;
    private T result;


    private DataUtilResult() {}

    private DataUtilResult(int code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result=result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    /**
     * 调用默认成功
     */
    public static <T> DataUtilResult<T> getSuccessResult(T result){
        return new DataUtilResult<T>(0, "success",result);
    }

    /**
     * 业务数据不存在
     * @param message 错误提示文字
     * @param result 结果
     */
    public static <T> DataUtilResult<T> getServiceErrorResult(String message, T result){
        if(null== message || "".equals(message)) {
            message = "业务数据不存在！";
        }
        return new DataUtilResult<T>(-2, message,result);
    }

    /**
     * 无访问权限
     * @param message 错误提示文字
     * @param result 结果
     */
    public static <T> DataUtilResult<T> getWithoutAccess(String message, T result){
        if(null== message || "".equals(message)){
            message = "该用户不在白名单内，无法访问页面";
        }
        return new DataUtilResult<T>(400,message,result);
    }

    /**
     * 系统发生未处理异常
     * @param message 错误提示文字
     * @param result 结果
     */
    public static <T> DataUtilResult<T> getFailureResult(String message, T result){
        if(null== message || "".equals(message)) {
            message = "系统发生未处理异常！";
        }
        return new DataUtilResult<T>(-1, message,result);
    }

    /**
     * 自定义异常
     * @param <T>
     * @return
     */
    public static <T> DataUtilResult<T> getCodeResult(ResultMsgEnum msgEnum, T result){
        String message = msgEnum.getMessage();
        Integer code = msgEnum.getCode();
        if(null== message || "".equals(message)){
            message = "系统发生未处理异常！";
        }
        code = code==null? -100 :code;
        return new DataUtilResult<T>(code,message,result);
    }

}
