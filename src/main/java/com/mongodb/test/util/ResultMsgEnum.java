package com.mongodb.test.util;

public enum ResultMsgEnum {
    RES_CODE_403(403,"权限不足！");

    private Integer code;
    private String message;

    ResultMsgEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
