package com.mongodb.test.service;

import com.mongodb.test.dao.UserRepository;
import com.mongodb.test.entity.User;
import com.mongodb.test.entity.req.UserReq;
import com.mongodb.test.util.DataUtilResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ldh
 * @create 2021-11-29 13:24
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    //@Autowired
   // private MongoTemplate mongoTemplate;

    /**
     * 保存用户
     * @param user
     */
    public DataUtilResult saveUser(User user){
        try{
            userRepository.save(user);
        return DataUtilResult.getSuccessResult("user插入成功");
        }catch (Exception e){
            e.printStackTrace();
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }
    }

    /**
     * 删除单个用户
     * @param id
     * @return
     */
    public DataUtilResult deleteUserById(String id){
        try{
            userRepository.deleteById(id);
            return DataUtilResult.getSuccessResult("user删除成功");
        }catch (Exception e){
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }
    }

    /**
     * 更新用户
     * @param user
     */
    public DataUtilResult updateUser(User user){
        try{
            userRepository.save(user);
            return DataUtilResult.getSuccessResult("user更新成功");
        }catch (Exception e){
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }

    }



    /**
     * 查询单个用户
     * @param id
     * @return
     */
    public DataUtilResult findUserById(String id){
        try{
            return DataUtilResult.getSuccessResult(userRepository.findById(id).get());
        }catch (Exception e){
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }
    }

    /**
     * 查询所有用户 并按照id倒序排序
     * @return
     */
    public DataUtilResult findUserList(){
        try{
            return DataUtilResult.getSuccessResult(userRepository.findAll(Sort.by(Sort.Direction.DESC, "id")));
        }catch (Exception e){
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }
    }


    /**
     * 多条件精确查询
     * @return
     */
    public DataUtilResult findPageList(UserReq userReq){
        try{
            User user = new User();
            user.setName(userReq.getName());
            user.setGender(userReq.getGender());
            ExampleMatcher matcher = ExampleMatcher.matching();
            Example<User> example = Example.of(user, matcher);
            Page<User> pageList = userRepository.findAll(example,PageRequest.of(userReq.getPage()-1, userReq.getSize()));
            return DataUtilResult.getSuccessResult(pageList);
        }catch (Exception e){
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }
    }


    /**
     * 多条件模糊查询
     * @return
     */
    public DataUtilResult findFuzzyPageList(UserReq userReq){
        try{
            User user = new User();
            user.setName(userReq.getName());
            user.setGender(userReq.getGender());
            //如果不设置匹配器默认精确匹配 这里设置为模糊匹配
            ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("name",ExampleMatcher.GenericPropertyMatchers.contains())
                    .withMatcher("gender",ExampleMatcher.GenericPropertyMatchers.contains());//采用开始匹配的方式查询

            Example<User> example = Example.of(user, matcher);
            Page<User> pageList = userRepository.findAll(example,PageRequest.of(userReq.getPage()-1, userReq.getSize()));
            return DataUtilResult.getSuccessResult(pageList);
        }catch (Exception e){
            return DataUtilResult.getFailureResult(e.getMessage(),null);
        }
    }

}
