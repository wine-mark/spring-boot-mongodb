package com.mongodb.test.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author Ldh
 * @create 2021-11-29 11:22
 */
@Data
@Document(collection = "User")//可省略(collection = "User")  如果省略则默认使用类名小写映射对象
public class User implements Serializable {

    @Id
    private String id;

    private String name;

    private String gender;

    private String phone;

}
