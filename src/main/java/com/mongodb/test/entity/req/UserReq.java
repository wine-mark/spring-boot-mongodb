package com.mongodb.test.entity.req;

import com.mongodb.test.entity.User;
import lombok.Data;

/**
 * @author Ldh
 * @create 2021-11-29 16:26
 */
@Data
public class UserReq extends User {

    private int page;
    private int size;
}
