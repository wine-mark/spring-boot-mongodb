package com.mongodb.test.dao;

import com.mongodb.test.entity.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author Ldh
 * @create 2021-11-29 13:21
 */
public interface UserRepository extends MongoRepository<User, String> {

    /**
     * 分页列表
     * @param pageable
     * @return
     */
    Page<User> findByGenderAndName(String Gender,String name,Pageable pageable);

}
