package com.mongodb.test.controller;

import com.mongodb.test.entity.User;
import com.mongodb.test.entity.req.UserReq;
import com.mongodb.test.service.UserService;
import com.mongodb.test.util.DataUtilResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Ldh
 * @create 2021-11-29 13:41
 */

@RestController
@RequestMapping("/mongo")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public DataUtilResult addUser(@RequestBody User user, HttpServletRequest request) {

        return userService.saveUser(user);
    }

    @PostMapping("/update")
    public DataUtilResult updateUser(@RequestBody User user) {

        return userService.updateUser(user);
    }

    @PostMapping("/delete")
    public DataUtilResult deleteUser(@RequestBody User user) {

        return userService.deleteUserById(user.getId());
    }
    @PostMapping("/find")
    public DataUtilResult findUser(@RequestBody User user) {

        return userService.findUserById(user.getId());
    }
    @PostMapping("/findAll")
    public DataUtilResult findAllUser(@RequestBody User user) {

        return userService.findUserList();
    }

    @PostMapping("/findList")
    public DataUtilResult findAllUserPage(@RequestBody UserReq userReq) {

        return userService.findPageList(userReq);
    }

    @PostMapping("/findFuzzyList")
    public DataUtilResult findFuzzyList(@RequestBody UserReq userReq) {

        return userService.findFuzzyPageList(userReq);
    }
}
